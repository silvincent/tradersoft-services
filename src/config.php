<?php

return [
    'operator_name' => env('TRADERSOFT_OPERATOR_NAME'),
    'secret_key' => env('TRADERSOFT_SECRET_KEY'),
    'api_url' => env('TRADERSOFT_API_URL'),
    'admin_api_url' => env('TRADERSOFT_ADMIN_API_URL'),
    'platform_url' => env('TRADERSOFT_PLATFORM_URL'),
    'partners_api_url' => env('TRADERSOFT_PARTNERS_API_URL'),
    'operator_key' => env('TRADERSOFT_OPERATOR_KEY'),
    'cookie_salt' => env('TRADERSOFT_COOKIE_SALT'),
    'base_domain' => env('TRADERSOFT_BASE_DOMAIN'),
    'affiliate_id' => env('TRADERSOFT_AFFILIATE_ID'),
    'partner_id' => env('TRADERSOFT_PARTNER_ID')
];
