<?php

namespace Tradersoft\Helpers;

class Curl
{
    public function post($url, $opt = [])
    {
        $request = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => self::buildQuery($opt),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            )
        );
        
        return self::processRequest($request);
    }
    
    public function put($url, $opt = array())
    {
        $request = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => self::buildQuery($opt),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            )
        );
        
        // return '{"data":{"leadId":181026352551,"firstName":"Pedro Vila Bela","lastName":"Pedro Vila Bela","email":"pedro.vilabela@gmail.com","username":"pedro.vilabela@gmail.com","phone":"+5581987208704","phone2":null,"cellphone":null,"departmentName":"Conversion","conversionStatusId":0,"retentionStatusId":0,"conversionStatusTitle":"New","retentionStatusTitle":"New","conversionCategoryTitle":"New","retentionCategoryTitle":"New","agentId":null,"agentName":null,"officeId":null,"officeName":null,"languageCode":"pt","countryType":"default","countryCode":"BR","town":"PE","postalCode":null,"address":null,"address2":null,"birthday":null,"marker":null,"currencyCode":"USD","ip":null,"tracking":null,"subtracking":null,"clickId":null,"param":null,"campaignId":null,"affiliateId":null,"hasDeposited":0,"balance":0,"balanceUsd":0,"lastTransactionTimestamp":null,"totalDepositAmount":0,"totalDepositAmountUsd":0,"isSuspended":0,"isVerified":0,"isTest":0,"accountTypeId":4,"verifiedChangedTimestamp":null,"registrationTimestamp":"2018-10-26T19:06:27+00:00","lastLoginTimestamp":null,"lastActiveTimestamp":"2018-10-26T19:06:28+00:00","lastOnlineTimestamp":null,"lastUpdateTimestamp":"2018-10-26T19:06:28+00:00","lastTradeTimestamp":null,"firstDepositTimestamp":null,"lastDepositTimestamp":null,"virtualGroupID":null},"timestamp":"2018-10-26T19:06:28+00:00"}';
        return self::processRequest($request);
    }

    public function get($url, $opt = array())
    {
        $request = array(
            CURLOPT_URL => $url.'?'.self::buildQuery($opt),
            CURLOPT_POST => 0,
            CURLOPT_RETURNTRANSFER => 1
        );
        
        return self::processRequest($request);
    }

    private static function processRequest($options = [])
    {
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        
        if (curl_errno($ch)) {
            $response = null;
        }
        
        curl_close($ch);
        return $response;
    }

    private static function buildQuery($opt) : string
    {
        $response = $opt;
        if (gettype($opt) == 'array') {
            $response = http_build_query($opt);
        }
        return $response;
    }
}
