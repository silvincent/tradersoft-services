<?php

namespace Tradersoft\Models;

use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public $id;
    public $username;
    public $passwordHash;
    public $email;
    public $name;
    public $crmHashId;
    public $traderHash;

    public function __construct($user)
    {
        $this->id = $user->crmHashId ?? $user->hash_id ?? null;
        $this->username = $user->username ?? $user->trader_username ?? null;
        $this->passwordHash = $user->passwordHash ?? $user->trader_password ?? null;
        $this->email = $user->email ?? null;
        $this->name = $user->firstName ?? $user->fname ?? null;
        $this->crmHashId = $user->crmHashId ?? null;
        $this->traderHash = $user->traderHash ?? null;
    }

    public function getAuthIdentifierName()
    {
        return 'crmHashId';
    }
    
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return $this->passwordHash;
    }

    public function getRememberToken()
    {
        return null;
    }

    public function setRememberToken($value)
    {
        return null;
    }

    public function getRememberTokenName()
    {
        return null;
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
