<?php

namespace Tradersoft\Clients;

use Carbon\Carbon;
use Tradersoft\Helpers\Curl;

class Admin
{
    private $curl;
    private $operatorName;
    private $operatorKey;
    private $statuses;
    private $acocuntTypes;
    private $virtualGroups;

    public $apiUrl;
    public $lastRequest;
    public $lastError;

    public function __construct()
    {
        $this->curl = new Curl();
        $this->operatorName = 'Diego.Tayler.Operator';
        $this->operatorKey = 'Rk2bxpz6Q2Ckd4RrgbteDmknbm';
        $this->apiUrl = 'http://api-crm.tradear.com/v1/';
        $this->lastError = null;
        $this->statuses = [];
        $this->accountTypes = [];
        $this->virtualGroups = [];
    }
    
    public function createLead(array $opt)
    {
        return $this->put('leads', $opt);
        // return $this->processResponse($response);
    }

    public function findLeads($opt = [])
    {
        $response = $this->get('leads', $opt);
        return $this->processResponse($response);
    }

    public function getLeadDetails($id, $opt = [])
    {
        $response = $this->get('leads/'.$id, $opt);
        return $this->processResponse($response);
    }
    
    public function getLeadLinks($id, $opt = [])
    {
        $response = $this->get("leads/$id/links", $opt);
        return $this->processResponse($response);
    }

    public function leadStatuses()
    {
        if (empty($this->statuses)) {
            $response = $this->get("leadStatuses");
            $statuses = $this->processResponse($response)->data;

            foreach ($statuses as $key => $status) {
                $this->statuses[$status->departmentName][$status->statusId] = [
                    'groupName' => $status->groupName,
                    'statusName' => $status->statusName
                ];
            }
        }
        return $this->statuses;
    }

    public function accountTypes()
    {
        if (empty($this->accountTypes)) {
            $result = $this->get('accountTypes');
            $types = $this->processResponse($result)->data;
            
            foreach ($types as $key => $type) {
                $this->accountTypes[$type->accountTypeId] = $type->accountTypeName;
            }
        }
        return $this->accountTypes;
    }

    public function virtualGroups()
    {
        if (empty($this->virtualGroups)) {
            $result = $this->get('virtualGroups');
            $groups = $this->processResponse($result)->data;
            
            foreach ($groups as $key => $group) {
                $this->virtualGroups[$group->virtualGroupID] = [
                    'department' => $group->departmentName,
                    'virtualGroupName' => $group->virtualGroupName
                ];
            }
        }

        return $this->virtualGroups;
    }

    public function postLeadDetails($id, $opt = [])
    {
        $response = $this->post("leads/$id", $opt);
        return $this->processResponse($response);
    }
    
    private function processResponse($result)
    {
        $response = json_decode($result);
        if (isset($response->errorMessage)) {
            $this->lastError = $response;
            $response = false;
        }
        
        return $response;
    }
    
    private function post($section, $opt = [])
    {
        $this->lastRequest = $opt;
        return $this->curl->post($this->apiUrl.$section, $this->checksum($opt));
    }

    private function put($section, $opt = [])
    {
        $this->lastRequest = $opt;
        return $this->curl->put($this->apiUrl.$section, $this->checksum($opt));
    }

    private function get($section, $opt = [])
    {
        $this->lastRequest = $opt;
        return $this->curl->get($this->apiUrl.$section, $this->checksum($opt));
    }

    private function checksum(array $params)
    {
        $date = Carbon::now();
        $params['timestamp'] = $date->toAtomString();
        $params = http_build_query($params);
        $query = 'operatorName='.$this->operatorName.'&'.$params;
        $checksum = hash('sha512', $this->operatorKey.$query);
        return $query.'&checksum='.$checksum;
    }
}
