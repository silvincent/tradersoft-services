<?php

namespace Tradersoft\Clients;

use Tradersoft\Helpers\Curl;

class Website
{
    private $ci;
    private $apiUrl;
    private $platformUrl;
    private $cookieSalt;
    private $secretKey;
    private $curl;
    private $ip;
    public $lastError;

    public function __construct($options = [])
    {
        $this->curl = new Curl();
        $this->apiUrl = config('tradersoft.api_url');
        $this->platformUrl = config('tradersoft.platform_url');
        $this->secretKey = config('tradersoft.secret_key');
        $this->cookieSalt = config('tradersoft.cookie_salt');
        $this->baseDomain = config('tradersoft.base_domain');
    }

    public function createAccount($opt)
    {
        $result = json_decode($this->put('/create-account', $opt));
        if (!isset($result->leadID)) {
            $this->lastError = $result;
        }
        return $result;
    }

    public function updateAccount($opt)
    {
        $result = json_decode($this->post('/update-account', $opt));
        if (!isset($result->leadID)) {
            $this->lastError = $result;
        }
        return $result;
    }

    public function login($opt)
    {
        $response = false;

        $opt['url'] = $this->platformUrl;
        $result = json_decode($this->post('/login-by-username', $opt));
        
        if (isset($result->leadInfo->crmHashId) && isset($result->leadInfo->traderHash)) {
            $this->unsetCookie();
            $this->setCookie($result->leadInfo->crmHashId, $result->leadInfo->traderHash);
            $response = $result->leadInfo;
        } else {
            $this->lastError = $result;
        }

        return $response;
    }

    public function logout()
    {
        return $this->unsetCookie();
    }

    public function getLead($id)
    {
        $opt = array(
            'accountNumber' => $id
        );
        $result = json_decode($this->post('/get-lead-by-account-number', $opt));
        return $result;
    }
    
    public function isLogged()
    {
        $response = false;
        
        if (isset($_COOKIE['trader-hash-id']) && isset($_COOKIE['trader-hash'])) {
            $traderHashId = explode('~', $_COOKIE['trader-hash-id']);
            $traderId = explode('~', $_COOKIE['trader-hash']);
            
            $response = ($_COOKIE['trader-hash-id'] == $this->salt('trader-hash-id', $traderHashId[1]))
                        && ($_COOKIE['trader-hash'] == $this->salt('trader-hash', $traderId[1]));
        }
        
        return $response;
    }

    public function getCountries($language = 'en')
    {
        $result = json_decode($this->post('/get-countries-all', array('lang' => $lang)));
        return $this->processResponse($result);
    }

    public function changePassword($leadId, $newPassword, $oldPassword, $sendEmailToClient = true)
    {
        $result = json_decode($this->post('/password-has-been-changed', array(
            'leadID' => $leadId,
            'newPassword' => $newPassword,
            'oldPassword' => $oldPassword,
            'sendEmailToClient' => (int)$sendEmailToClient
        )));

        return $this->processResponse($result);
    }

    public function resetPassword($email)
    {
        $result = json_decode($this->post('/try-forgot-password', array(
            'email' => $email
        )));

        return $this->processResponse($result);
    }

    public function requestWithdrawal($email, $amount)
    {
        $result = json_decode($this->post('/withdrawal-request', array(
            'username' => $email,
            'amount' => $amount
        )));

        return $this->processResponse($result);
    }

    public function getWithdrawalRequests($leadId)
    {
        $result = json_decode($this->post('/get-withdrawal-request-by-lead-id', array(
            'leadID' => $leadId
        )));

        return $this->processResponse($result);
    }

    public function cancelWithdrawalRequest($requestId, $leadId)
    {
        $result = json_decode($this->post('/cancel-withdrawal-request', array(
            'requestId' => $requestId,
            'leadId' => $leadId
        )));

        return $this->processResponse($result);
    }

    private function processResponse($result)
    {
        $response = true;

        if (!isset($result->returnCode) || $result->returnCode != 0) {
            $response = false;
            $this->lastError = $result;
        }
        
        return $response;
    }

    private function unsetCookie()
    {
        $response = false;
        if (!headers_sent()) {
            setcookie('trader-hash-id', null, time()-3600, '/', $this->baseDomain, false, true);
            setcookie('trader-hash', null, time()-3600, '/', $this->baseDomain, false, true);
            $response = true;
        }
        return $response;
    }

    private function setCookie($crmHashId, $traderHash)
    {
        $response = false;
        if (!headers_sent()) {
            $hashIdSalt = $this->salt('trader-hash-id', $crmHashId);
            $hashSalt = $this->salt('trader-hash', $traderHash);
    
            $result = setcookie('trader-hash-id', $hashIdSalt, time()+3600, '/', $this->baseDomain, false, true);
            setcookie('trader-hash', $hashSalt, time()+3600, '/', $this->baseDomain, false, true);
            $response = true;
        }
        return $response;
    }

    private function post($section, $opt)
    {
        $opt['secretKey'] = $this->secretKey;
        return $this->curl->post($this->apiUrl.$section, $opt);
    }

    private function put($section, $opt)
    {
        $opt['secretKey'] = $this->secretKey;
        return $this->curl->put($this->apiUrl.$section, $opt);
    }

    private function get($section, $opt)
    {
        $opt['secretKey'] = $this->secretKey;
        return $this->curl->get($this->apiUrl.$section, $opt);
    }

    private function salt($name, $value)
    {
        $agent = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : 'unknown';
        $salt = $this->cookieSalt;
        return sha1($agent . $name . $value . $salt).'~'.$value;
    }
}
