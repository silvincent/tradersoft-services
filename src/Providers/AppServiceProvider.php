<?php

namespace Tradersoft\Providers;

use Tradersoft\Events\UserLogin;
use Illuminate\Support\Facades\Auth;
use Tradersoft\Events\EventListener;
use Tradersoft\Providers\UserProvider;
use Tradersoft\Providers\EventServiceProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class AppServiceProvider extends LaravelServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('tradersoft', function ($app, array $config) {
            return new UserProvider();
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__.'/../config.php' => config_path('tradersoft.php')
        ]);

        $this->app->register(EventServiceProvider::class);
    }
}
