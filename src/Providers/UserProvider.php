<?php

namespace Tradersoft\Providers;

use Tradersoft\Models\User;
use Tradersoft\Clients\Website;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Contracts\Auth\UserProvider as LaravelUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class UserProvider implements LaravelUserProvider
{

    private $client;

    public function __construct()
    {
        $this->client = new Website();
    }

    public function retrieveById($identifier)
    {
        $response = $this->client->getLead($identifier);
        $user = new User($response->leadInfo);
        return $user;
    }
    
    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->client->logout();
        return null;
    }

    public function retrieveByCredentials(array $credentials)
    {
        $response = $this->client->login([
            'username' => $credentials['email'],
            'password' => $credentials['password'],
            'ip' => Request::ip()
        ]);

        $user = new User($response);
        return $user;
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $user->getAuthIdentifier() != null;
    }
}
