<?php

namespace Tradersoft\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Log;

class DeleteCookie
{
    public function __construct()
    {
        //
    }

    public function handle(Logout $event)
    {
        Log::info('Usuario se desloguea de la plataforma: '.json_encode($event));
    }
}
