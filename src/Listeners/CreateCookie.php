<?php

namespace Tradersoft\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Log;

class CreateCookie
{
    public function __construct()
    {
        //
    }

    public function handle(Login $event)
    {
        $user = $event->user;
        Log::info('Usuario se loguea a la plataforma: '.json_encode($event));
    }
}
